<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', [\App\Http\Controllers\InicioController::class, 'inicio'] )->name('home');

Route::get('comics', [\App\Http\Controllers\ComicController::class, 'index'])->name('comics.index');

Route::get('comics/cat/{categoria}', [\App\Http\Controllers\ComicController::class, 'categorias'])->name('comics.categorias');

Route::post('comics/busqueda', [\App\Http\Controllers\ComicController::class, 'busqueda'])->name('comics.busqueda'); 
Route::get('comics/{comic}', [\App\Http\Controllers\ComicController::class, 'show'])->name('comics.show');  
 


Route::get('comics/modificar/{comic}', [\App\Http\Controllers\ComicController::class, 'edit'])->name('comics.edit');  
Route::put('comics/modificar/{comic}', [\App\Http\Controllers\ComicController::class, 'update'])->name('comics.update'); 


Route::get('comics/com/publicar/', [\App\Http\Controllers\ComicController::class, 'create'])->name('comics.create');
Route::post('comics/com/publicar/', [\App\Http\Controllers\ComicController::class, 'store'])->name('comics.store'); 


Route::middleware(['auth:sanctum', 'verified'])->get('/dashboard', function () {
    return view('dashboard');
})->name('dashboard');


