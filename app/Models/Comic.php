<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Comic extends Model {

    use HasFactory;

    protected $table = "comics";
    public $timestamps = false;
    

    public function categorias() {
        return $this->belongsTo(Categoria::class);
    }

      public function tags()
      {
      return $this->belongsToMany(Tag::class);
      
      } 

}
