<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Comic;

class ComicController extends Controller {

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index() {
        //ESTA LINEA ME DEVUELVE LA VISTA INDEX CON UN ARRAY DE COMICS
        $comics = \App\Models\Comic::inRandomOrder()->limit(15)->get();
        return view('comics.index', compact('comics'));
        
    }

    public function categorias(\App\Models\Categoria $categoria) {
        //ESTA LINEA ME DEVUELVE LA VISTA INDEX CON UN ARRAY DE COMICS
        $comics = \App\Models\Comic::all()->where('categoria_id', "=", $categoria->id);

        return view('comics.generos', compact('comics'));
    }

    public function edit($id) {
        $comic = Comic::find($id);
        return view('comics.modificar', ['comic' => $comic]);
    }
    
    public function busqueda(Request $req) {
        $comics = Comic::where("nombre", "like", "%".$req->buscador ."%")->get();
        
        return view('comics.busqueda', ['comics' => $comics]);
    }

    public function show(Comic $comic) {

        return view("comics.show", ["comic" => $comic]);
    }

    public function create() {
        return view("comics.publicar");
    }

    public function store(Request $request) {
        /*
          Esta es la mejor forma de hacerlo: */

        //return $request->all(); //esto me devuelve todos los datos
        $comic=new Comic();

        $comic->nombre = $request->nombre;
        $comic->autor = $request->autor;
        $comic->descripcion = $request->descripcion;
        $comic->paginas = $request->paginas;
        $comic->tipografia = $request->tipografia;
        $comic->tag_id = $request->tag;
        $comic->categoria_id = $request->categoria;
        $comic->logo= $request->logo->store('', 'comics');
        $comic->save();
        return redirect()->route("comics.show", $comic);
    }

    public function update(Request $request, Comic $comic) {


        $comic->nombre = $request->nombre;
        $comic->autor = $request->autor;
        $comic->descripcion = $request->descripcion;
        $comic->paginas = $request->paginas;
        $comic->tipografia = $request->tipografia;
        $comic->logo= $request->logo->store('', 'comics');
        $comic->save();

        return view("comics.show", ["comic" => $comic])->with("mensaje", "Ha habido un error");
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id) {
        //
    }

}
