<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;

class CategoriaSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
        
    public function run()
    {
           $categoria=new \App\Models\Categoria();
     //      $categoria->comic_id= \App\Models\Comic::all()->random()->id;
           $categoria->nombre= "kodomo";
           $categoria->save();
           
           $categoria1=new \App\Models\Categoria();
       //    $categoria1->comic_id= \App\Models\Comic::all()->random()->id;
           $categoria1->nombre= "shonen";
           $categoria1->save();
           
           $categoria2=new \App\Models\Categoria();
         //  $categoria2->comic_id= \App\Models\Comic::all()->random()->id;
           $categoria2->nombre= "shojo";
           $categoria2->save();
           
           $categoria3=new \App\Models\Categoria();
         //  $categoria3->comic_id= \App\Models\Comic::all()->random()->id;
           $categoria3->nombre= "seinen";
           $categoria3->save();
           
           $categoria4=new \App\Models\Categoria();
           //$categoria4->comic_id= \App\Models\Comic::all()->random()->id;
           $categoria4->nombre= "josei";
           $categoria4->save();
           
           $categoria5=new \App\Models\Categoria();
           //$categoria5->comic_id= \App\Models\Comic::all()->random()->id;
           $categoria5->nombre= "comica";
           $categoria5->save();
           
           $categoria6=new \App\Models\Categoria();
           //$categoria6->comic_id= \App\Models\Comic::all()->random()->id;
           $categoria6->nombre= "belico";
           $categoria6->save();
           
           $categoria7=new \App\Models\Categoria();
           //$categoria7->comic_id= \App\Models\Comic::all()->random()->id;
           $categoria7->nombre= "aventuras";
           $categoria7->save();
           
           $categoria8=new \App\Models\Categoria();
          // $categoria8->comic_id= \App\Models\Comic::all()->random()->id;
           $categoria8->nombre= "fantastico";
           $categoria8->save();
           

    }
}
