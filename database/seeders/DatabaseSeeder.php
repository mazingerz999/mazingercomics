<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use App\Models\Comic;
use Illuminate\Support\Facades\DB;
class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        // \App\Models\User::factory(10)->create();
        
      
      // Comic::factory(20)->create();
     
 DB::table('categorias')->delete();
 $this->call(CategoriaSeeder::class);
 DB::table('tags')->delete();
 $this->call(TagSeeder::class);
  DB::table('comics')->delete();
 $this->call(ComicSeeder::class);  
   
 DB::table('users')->delete();
 $this->call(UserSeeder::class);
 \App\Models\User::factory(5)->create();
 

    }
}
