<?php

namespace Database\Seeders;

use App\Models\Tag;
use Illuminate\Database\Seeder;


class ComicSeeder extends Seeder
{

     private $comics = array(
         array(
            'nombre' => 'Doraemon',
            'autor' => 	'Fujiko F. Fujio',
            'paginas' => 20,
            'descripcion' => 'Doraemon es una serie de'
             . ' manga escrita e ilustrada por Fujiko Fujio. '
             . 'La cual trata sobre un gato robot cósmico azul '
             . 'llamado Doraemon que viene del futuro con el objetivo'
             . ' de ser amigo del descentrado Nobita, un chico muy '
             . 'perdido entre los estudios, algo torpe con sus amigos,'
             . ' desafortunado, vago, holgazán y desgraciado. Doraemon'
             . ' viene del siglo XXII y ayudará a Nobita en '
             . 'todos sus problemas cotidianos con sus padres'
             . ' y amigos con la ayuda de sus inventos que saca'
             . ' de un bolsillo mágico blanco pegado a su barriga.',
            'tipografia' => 'Oriental',
            'logo' => 'doraemon.jpg',
                   ),
         array(
            'nombre' => 'Spider-Man',
            'autor' => 	'Brian Michael Bendis',
            'paginas' => 20,
            'descripcion' => 'Spider-Man está columpiándose, reflexionando sobre '
             . 'que New York es increíble con todo lo que le ha pasado hasta'
             . ' entonces, cuando, luego de detener un asalto automovilístico, '
             . 'encuentra un portal inter-dimensional a la dimensión Ultimate'
             . ' Marvel en una bodega abandonada, y descubre que quien está detrás'
             . ' de todo esto: Misterio, diciéndole que ya se perdió la'
             . ' primera vez que Spider-Man murió, y no quiere perderse ésta '
             . '( haciendo referencia a Ultimate Spider-Man) y Spidey dice esta'
             . ' loco; pero cuando Araña está a punto de detenerlo, este le '
             . 'dispara con un último intento de salirse con la suya, por'
             . ' error le da al portal, y créa una sobrecarga que arrastra a'
             . ' Spider-Man a la realidad Ultimate, y Misterio se queda en '
             . 'la realidad 616  Amazing, inconsciente.',
            'tipografia' => 'Occidental',
            'logo' => 'spider.jpg',
                   ),
         array(
            'nombre' => 'Mi vecino Totoro',
            'autor' => 	'Hayao Miyazaki',
            'paginas' => 20,
            'descripcion' => 'Mi vecino Totoro (となりのトトロ Tonari'
             . ' no Totoro?) es una película de animación japonesa'
             . ' de 1988 escrita y dirigida por Hayao Miyazaki y'
             . ' producida por Studio Ghibli. El filme —protagonizado'
             . ' por los actores Noriko Hidaka, Chika Sakamoto y Hitoshi'
             . ' Takagi— cuenta la historia de una familia y sus'
             . ' interacciones con un espíritu del bosque al que '
             . 'llaman "Totoro", en un Japón de la posguerra. Mi vecino'
             . ' Totoro ganó el premio Anime Grand Prix de Animage,'
             . ' el Mainichi Film Award y el Kinema Junpo Award en'
             . ' la categoría de "Mejor película" en 1988. También'
             . ' recibió un premio especial en los Blue Ribbon Awards '
             . 'de ese mismo año. Es el cuarto largometraje realizado'
             . ' por Studio Ghibli, del cual Totoro es su logotipo.'
             . ' Fue elegida por la revista británica Time Out como'
             . ' la mejor película de animación de la historia​',
            'tipografia' => 'Oriental',
            'logo' => 'totoro.jpg',
                   ),
         array(
            'nombre' => 'Hamtaro ',
            'autor' => 	'Ritsuko Kawai',
            'paginas' => 20,
            'descripcion' => 'Hamtaro es un manga obra de Ritsuko Kawai'
             . ' de 3 volúmenes publicados en 1997 por la editorial'
             . ' Shogakukan, del que se ha producido una serie anime en'
             . ' la que trabajó Gosho Aoyama diseñando los personajes'
             . ' humanos, también ha producido 4 OVAs, 4 películas, y '
             . 'algunos videojuegos. Está protagonizado por un hámster'
             . ' llamado Hamtaro y sus amigos, los'
             . ' Ham-Hams. El eslogan de la serie es Pequeños hámsters,'
             . ' grandes aventuras.',
            'tipografia' => 'Oriental',
            'logo' => 'hamtaro.jpg',
                   ),
         array(
            'nombre' => 'Pokémon ',
            'autor' => 	'Masamitsu Hidaka',
            'paginas' => 20,
            'descripcion' => 'Pokémon, abreviando su nombre original'
             . ' Pocket Monsters Monstruos de bolsillo), es un anime'
             . ' metaserial creado por Satoshi Tajiri, '
             . 'Junichi Masuda y Ken Sugimori, que narra la'
             . ' historia de Ash Ketchum, un joven entrenador '
             . 'Pokémon de Pueblo Paleta que comienza un viaje '
             . 'para alcanzar su sueño, ser un Maestro Pokémon. '
             . 'La serie está basada en la saga de videojuegos de'
             . ' Pokémon también creada por Satoshi Tajiri,'
             . ' desarrollada por Game Freak y distribuida por Nintendo'
             . ', que aparecieron por primera vez en el mercado'
             . ' japonés el 27 de febrero de 1996.',
            'tipografia' => 'Oriental',
            'logo' => 'pokemon.jpg',
                   ),
         array(
            'nombre' => 'Dragon Ball',
            'autor' => 	'Akira Toriyama',
            'paginas' => 20,
            'descripcion' => 'Su trama describe las aventuras de Goku,'
             . ' un guerrero saiyajin, cuyo fin es proteger a la Tierra'
             . ' de otros seres que quieren conquistarla y exterminar'
             . ' a la humanidad. Conforme transcurre la trama, conoce'
             . ' a otros personajes que le ayudan en este propósito.'
             . ' El nombre de la serie proviene de unas esferas mágicas'
             . ' que al ser reunidas invocan a un dragón que concede'
             . ' deseos. En varias ocasiones resultan útiles tanto para'
             . ' Gokū y sus amigos como para la humanidad, aunque '
             . 'también son procuradas de forma constante por algunos'
             . ' seres malignos.',
            'tipografia' => 'Oriental',
            'logo' => 'dragon.jpg',
                   ),
         array(
            'nombre' => 'Devilman',
            'autor' => 	'Gō Nagai',
            'paginas' => 20,
            'descripcion' => 'El estudiante de secundaria Akira Fudo'
             . ' es informado por su mejor amigo, Ryo Asuka, qu'
             . 'e una antigua raza de demonios ha regresado para'
             . ' conquistar el mundo de los humanos. Creyendo que'
             . ' la única forma de derrotar a los demonios es'
             . ' apoderarse de sus poderes, Ryō le sugiere a Akira'
             . ' que se una con un demonio. Al tener éxito al hacerlo,'
             . ' Akira se transforma en Devilman, poseyendo los poderes'
             . ' de un demonio pero reteniendo el alma y corazón '
             . 'de un humano.',
            'tipografia' => 'Oriental',
            'logo' => 'devilman.jpg',
                   ),
         array(
            'nombre' => 'One Piece',
            'autor' => 	'Eiichirō Oda',
            'paginas' => 20,
            'descripcion' => ' Monkey D. Luffy, un niño de 7 años,'
             . ' sueña con ser un pirata, y por ello, decide'
             . ' pedirle a Shanks unirse a su tripulación, cosa que'
             . ' Shanks rechaza. Luffy, por error, se come la'
             . ' fruta Gomu Gomu, una fruta del diablo que le'
             . ' permitió convertirse en un hombre de goma, pero'
             . ' quitándole la capacidad de poder nadar para siempre.'
             . ' Tras un incidente con unos bandidos, en el que'
             . ' Shanks no pelea, Luffy le echa en cara que es un'
             . ' cobarde, y aunque decide vengar el honor de Shanks'
             . ' y su tripulación, es capturado por los bandidos y'
             . ' arrojado al mar. Es rescatado por Shanks, quien,'
             . ' lamentablemente, perdió el brazo izquierdo.',
            'tipografia' => 'Oriental',
            'logo' => 'onepiece.jpg',
                   ),
         array(
            'nombre' => 'Bleach',
            'autor' => 	'Tite Kubo',
            'paginas' => 20,
            'descripcion' => 'Ichigo Kurosaki es un adolescente japonés'
             . ' aparentemente ordinario que tiene la facultad de interactuar'
             . ' con los espíritus.2​ Una noche, Ichigo se topa con un ente que '
             . 'no había conocido antes una shinigami —personificación japonesa'
             . ' del Dios de la muerte— llamada Rukia Kuchiki, quien se sorprende'
             . ' que pueda verla y de su poder espiritual latente. Sin embargo,'
             . ' su conversación es interrumpida por la aparición de un hollow,'
             . ' un espíritu maligno que deja malheridos a la familia Kurosaki'
             . ' y a la propia Rukia al tratar de proteger a Ichigo, esta intenta'
             . ' traspasarle parte de sus poderes a Ichigo para que pueda'
             . ' enfrentarse al hollow en igualdad de condiciones. No obstante,'
             . ' Ichigo sin darse cuenta los absorbe casi '
             . 'por completo y logra vencer con facilidad al espíritu.',
            'tipografia' => 'Oriental',
            'logo' => 'bleach.jpg',
                   ),
         array(
            'nombre' => 'Kyou, Koi wo Hajimemasu',
            'autor' => 	'Kanan Minami',
            'paginas' => 20,
            'descripcion' => 'Tsubaki Hibino es una chica que'
             . ' estudia secundaria, algo despistada y normalita,'
             . ' pero que esconde un gran talento el cual es hacer peinados,'
             . ' aunque no se peine muy bonito ella misma. Por otro lado'
             . ' esta Kyouta Tsubaki, el playboy de la secundaria que al'
             . ' intimidar a nuestra protagonista '
             . 'provocará que esta se vengue usando su talento',
            'tipografia' => 'Oriental',
            'logo' => 'kyou.jpg',
                   ),
         array(
            'nombre' => 'Akira ',
            'autor' => 	'Katsuhiro Ōtomo',
            'paginas' => 20,
            'descripcion' => 'La historia se desarrolla en el año 2019 en Neo-Tokio'
             . ', una ciudad reconstruida tras sufrir los devastadores efectos '
             . 'de una presunta explosión nuclear que desencadena la tercera guerra'
             . ' mundial. El gobierno ejerce un control represivo sobre la ciudad y'
             . ' experimenta sobre unos niños con poderes psíquicos latentes, '
             . 'aplicándoles fármacos para potenciarlos, estos contribuyen con '
             . 'predicciones para mantener la paz. Kaneda y Tetsuo son miembros'
             . ' de una pandilla de motociclistas llamada "The capsules" que tienen'
             . ' entre otras aficiones'
             . ' participar en peleas callejeras contra otras bandas,',
            'tipografia' => 'Oriental',
            'logo' => 'akira.jpg',
                   ),
         array(
            'nombre' => 'Death Note',
            'autor' => 	'Tsugumi Ōba',
            'paginas' => 20,
            'descripcion' => 'a historia es protagonizada por Light Yagami,'
             . ' un estudiante sobresaliente de Japón que tiene una perspectiva'
             . ' «aburrida» de la vida y quien, casi al acabar el primer capítulo,'
             . ' empieza a considerar el mundo como un lugar «podrido».'
             . ' Su vida sufre un cambio radical, cuando encuentra un extraño'
             . ' cuaderno sobrenatural llamado «Death Note», tendido en el suelo.'
             . ' Detrás de la portada de dicho cuaderno había instrucciones sobre '
             . 'su uso, donde decía que si se escribía el nombre de una persona y se '
             . 'visualizaba'
             . ' mentalmente el rostro de esta, moriría de un ataque al corazón.',
            'tipografia' => 'Oriental',
            'logo' => 'deathnote.jpg',
                   ),
         array(
            'nombre' => 'Shōwa Genroku Rakugo Shinjū',
            'autor' => 	'Haruko Kumota',
            'paginas' => 20,
            'descripcion' => 'Yotarō, el protagonista de la historia,'
             . ' acaba de salir de la cárcel y solo tiene un objetivo:'
             . ' convertirse en el aprendiz de Yakumo Yurakutei VIII, un'
             . ' maestro del rakugo, arte japonés de contar historias de'
             . ' monólogo. Tras ser aceptado por Yakumo conoce a su protegida,'
             . ' Konatsu, hija de otro gran exponente muerto de manera trágica:'
             . ' Sukeroku Yurakutei II. El joven Yotarō se esfuerza'
             . ' para dar lo mejor de sí y encontrar su propio estilo de rakugo.',
            'tipografia' => 'Oriental',
            'logo' => 'showa.jpg',
                   ),
         array(
            'nombre' => 'Zipi y Zape',
            'autor' => 	'José Escobar ',
            'paginas' => 20,
            'descripcion' => 'Este par de gemelos, que se distinguen entre'
             . ' sí por ser uno moreno (Zape) y otro rubio (Zipi), se caracterizaban'
             . ' principalmente por las endiabladas travesuras en que incurrían a la'
             . ' menor ocasión. Los mismos nombres de los protagonistas provienen de'
             . ' la palabra zipizape, que significa "alboroto".'
             . ' La personalidad de Zipi y Zape es muy simple',
            'tipografia' => 'Occidiental',
            'logo' => 'zipizape.jpg',
                   ),
         array(
            'nombre' => 'Leones de Bagdad',
            'autor' => 	'Vaughan ',
            'paginas' => 20,
            'descripcion' => 'En abril de 2003, una manada de leones se escapó del'
             . ' zoológico de Bagdad durante un bombardeo del ejército estadounidense.'
             . 'Los cuatro leones -perdidos, confusos y hambrientos, pero al fin'
             . ' libres- vagaron por las diezmadas calles de Bagdad'
             . ' en una lucha desesperada por sobrevivir.Inspirándose en hechos....',
            'tipografia' => 'Occidiental',
            'logo' => 'leones.jpg',
                   ),
         array(
            'nombre' => 'Lucky Luke',
            'autor' => 	'Morris',
            'paginas' => 20,
            'descripcion' => 'Lucky Luke es conocido por ser'
             . 'más rápido que su propia sombra '
             . 'y es un vaquero que hace frente al crimen y'
             . ' a la injusticia deteniendo forajidos, escoltando'
             . ' caravanas de pioneros mormones y ejerciendo de'
             . ' mediador del Gobierno de los EE. UU. en misiones'
             . ' diplomáticas particularmente delicadas. El cowboy'
             . ' se caracterizaba en los inicios de la serie por tener'
             . ' constantemente un cigarrillo en la boca, aunque Morris'
             . ' lo sustituyó por una pajita en 1983, lo que le valió el '
             . 'reconocimiento de la Organización Mundial de la Salud.',
            'tipografia' => 'Occidiental',
            'logo' => 'lucky.jpg',
                   ),
         array(
            'nombre' => 'Batman',
            'autor' => 	'Bill Finger, Tom King y Bob Kane',
            'paginas' => 20,
            'descripcion' => 'Batman es la identidad secreta de Bruce Wayne,'
             . ' un empresario multimillonario, galán y filántropo. Presenció'
             . ' el asesinato de sus padres cuando era niño lo marcó profundamente'
             . ' y lo llevó a entrenarse en la perfección física e intelectual para'
             . ' ponerse un disfraz de murciélago con el fin de combatir el crimen.',
            'tipografia' => 'Occidiental',
            'logo' => 'batman.jpg',
                   ),
         array(
            'nombre' => 'Constantine',
            'autor' => 	'Alan Moore',
            'paginas' => 20,
            'descripcion' => 'Conocido como Hellblazer, Constantine es un'
             . ' detective de lo oculto y timador, de clase trabajadora,'
             . ' con residencia en Londres. Fumador compulsivo, notorio por'
             . ' su cinismo sin límites, se caracteriza por su sarcasmo'
             . ' inexpresivo, así como por su capacidad para la manipulación'
             . ' despiadada. Sin embargo, también es un'
             . ' apasionado humanista, conducido por un intenso deseo de hacer el bien.',
            'tipografia' => 'Occidiental',
            'logo' => 'constantine.jpg',
                   ),
         array(
            'nombre' => 'Conan',
            'autor' => 	'Barry Windsor-Smith',
            'paginas' => 20,
            'descripcion' => 'La Era Hyboria se encuentra con la Era Marvel'
             . ' en esta impresionante galería de ilustraciones de Conan! El'
             . ' guerrero cimmerio dejó su huella en los cómics de la Marvel de'
             . ' los años setenta y sus'
             . ' cómics son recordados hoy día como una de las cimas de la editorial.',
            'tipografia' => 'Occidiental',
            'logo' => 'conan.jpg',
                   ),
         array(
            'nombre' => 'La mazmorra',
            'autor' => 	'Lewis Trondheim',
            'paginas' => 20,
            'descripcion' => 'Esta saga transcurre en un mundo llamado Terra'
             . ' Amata alrededor de un castillo conocido por el '
             . 'nombre de La Mazmorra, durante tres épocas muy '
             . 'diferenciadas entre sí.',
            'tipografia' => 'Occidiental',
            'logo' => 'makinavaja.jpg',
                   ),
         array(
            'nombre' => 'Fábulas',
            'autor' => 	'Bill Willingham',
            'paginas' => 20,
            'descripcion' => 'Fábulas es una serie de historietas del sello'
             . ' Vertigo, de DC Comics, creado y escrito por Bill Willingham,'
             . ' que se basa en'
             . ' personajes clásicos de los cuentos de hadas y el folklore popular.',
            'tipografia' => 'Occidiental',
            'logo' => 'fabulas.jpg',
                   ),
         array(
            'nombre' => 'Las ciudades oscuras',
            'autor' => 	'François Schuiten',
            'paginas' => 20,
            'descripcion' => 'La serie se desarrolla en un continente'
             . ' imaginario ubicado en un mundo paralelo, en principio'
             . ' invisible al nuestro, en el que se mezclan multitud de'
             . ' influencias, desde el surrealismo metafísico Borgesiano'
             . ' hasta la literatura de Julio Verne (principalmente sus viajes'
             . ' extraordinarios). En la serie destacan los detallados'
             . ' diseños arquitectónicos de Schuiten para las distintas '
             . 'Ciudades oscuras, que son en su mayor parte'
             . ' reinterpretaciones o versiones "oscuras"'
             . ' de ciudades reales, como Brüsel de Bruselas o Pâhry de París.',
            'tipografia' => 'Occidiental',
            'logo' => 'ciudades.jpg',
                   ),
         array(
            'nombre' => 'El botones Sacarino',
            'autor' => 	'Francisco Ibáñez',
            'paginas' => 20,
            'descripcion' => 'Sacarino es un chico ingenuo y un algo torpe,'
             . ' botones de un periódico llamado El aullido vespertino que'
             . ' posteriormente pasaría a serlo del DDT. Tiene múltiples tareas'
             . ' que realizar: limpiar y ordenar los despachos, llenar de tinta'
             . ' los tinteros, hacer todo tipo de recados, etc.'
             . ' Pero Sacarino siempre intenta escaquearse del trabajo para dormir.',
            'tipografia' => 'Occidiental',
            'logo' => 'botonessacarino.jpg',
                   ),


    );

     public function run() {
        foreach ($this->comics as $comic) {
            $a = new \App\Models\Comic();
            $a->nombre = $comic['nombre'];
            $a->autor = $comic['autor'];
            $a->paginas = $comic['paginas'];
            $a->descripcion = $comic['descripcion'];
            $a->tipografia = $comic['tipografia'];
            $a->logo = $comic['logo'];
            $a->categoria_id = \App\Models\Categoria::all()->random()->id;

            $a->save();
            $a->tags()->attach([Tag::all()->random()->id]);
        }
        $this->command->info('Tabla animales inicializada con datos');
    }
}
