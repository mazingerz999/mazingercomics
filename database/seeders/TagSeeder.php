<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;

class TagSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *       
     * @return void
     */
    public function run()
    {
           $tag1=new \App\Models\Tag();
           
           $tag1->nombre= "vampiros";
           $tag1->save();
           
           $tag2=new \App\Models\Tag();
           
           $tag2->nombre= "militar";
           $tag2->save();
           
           $tag3=new \App\Models\Tag();
          
           $tag3->nombre= "lucha";
           $tag3->save();
           
           $tag4=new \App\Models\Tag();
          
           $tag4->nombre= "carreras";
           $tag4->save();
           
           $tag5=new \App\Models\Tag();
          
           $tag5->nombre= "asesino";
           $tag5->save();
                     
           
           $tag6=new \App\Models\Tag();
       
           $tag6->nombre= "magia";
           $tag6->save();
           
           $tag7=new \App\Models\Tag();
          
           $tag7->nombre= "policia";
           $tag7->save();
           
           $tag8=new \App\Models\Tag();
        
           $tag8->nombre= "instituto";
           $tag8->save();
           
           $tag9=new \App\Models\Tag();
       
           $tag9->nombre= "futuro";
           $tag9->save();
           
           $tag10=new \App\Models\Tag();
   
           $tag10->nombre= "musica";
           $tag10->save();
           
           $tag11=new \App\Models\Tag();
     
           $tag11->nombre= "romance";
           $tag11->save();
           
           $tag12=new \App\Models\Tag();
         
           $tag12->nombre= "espacio";
           $tag12->save();
    }
}
