<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateComicsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('comics', function (Blueprint $table) {
            $table->id();
            $table->string("nombre")->nullable();
            $table->string("autor")->nullable();
            $table->double('paginas', 6,1)->nullable();
            $table->longText('descripcion')->nullable();
            $table->string("tipografia")->nullable();
            $table->string('logo')->nullable();
            $table->unsignedBigInteger("categoria_id");


            $table->foreign("categoria_id")->references("id")->on("categorias");
            $table->timestamps();

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('comics');
    }
}
