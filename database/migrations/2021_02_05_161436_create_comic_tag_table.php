<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateComicTagTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('comic_tag', function (Blueprint $table) {

            $table->unsignedBigInteger('comic_id');
            $table->foreign("comic_id")->references("id")->on("comics");
            $table->unsignedBigInteger('tag_id');
            $table->foreign("tag_id")->references("id")->on("tags");
            $table->primary(array("comic_id", "tag_id"));
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('comic_tag');
    }
}
