<?php

namespace Database\Factories;

use App\Models\Comic;
use Illuminate\Database\Eloquent\Factories\Factory;

class ComicFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = Comic::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            'nombre' => $this->faker->name,
            'autor' => $this->faker->safeEmail,
            'paginas' => $this->faker->words,
            'descripcion' => $this->faker->text,
            'tipografia' => $this->faker->title,
            'logo' => $this->faker->image()           
        ];
    }
}
