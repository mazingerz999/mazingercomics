@extends('layouts.master')
@section('titulo')
    Mazinger Comics
@endsection
@section('contenido')
<!--
@if(session("mensaje"))
<div class="alert alert-info">
    {{session("mensaje")}}
</div>

@endif-->
<style>

    .wrapper {
  /*This part is important for centering*/
  display: flex;
  align-items: center;
  justify-content: center;
}

.typing-demo {
  width: 15ch;
  animation: typing 1.5s steps(22), blink .5s step-end infinite alternate;
  white-space: nowrap;
  overflow: hidden;
  border-right: 3px solid;
  font-family: monospace;
  font-size: 3em;
}


@keyframes typing {
  from {
    width: 0
  }
}

@keyframes blink {
  50% {
    border-color: transparent
  }
}
</style>
<br>
<div class="wrapper">
    <div class="typing-demo">
     Recomendaciones
    </div>
</div>
 <div class="row justify-content-center">

    <!-- Recorro con un for each el array de animales -->
@foreach( $comics as  $comic )
<div class="card  border-success mb-1 p-2 m-3" style="width: 19rem;">
    <!-- Cojo la ruta animal y uso la imagen -->
<img src="{{asset('assets/imagenes')}}/{{$comic->logo}}" style="height:300px"/>

<div class="card-body">

    <h5 class="card-title">- Nombre: {{$comic->nombre}}</h5>
    <p class="card-text">- Autor:  {{$comic->autor}}</p>
    <p class="card-text">- Nº Paginas: {{$comic->paginas}}</p>
    <p class="card-text">- Tipografia: {{$comic->tipografia}}</p>
    <p class="card-text">- Categoria: {{$comic->categorias}}</p>
    @foreach($comic->tags as $tag)
        <p class="card-text">- Tags: {{$tag->nombre}}</p>
        @endforeach
    </p>

    <hr>
  <a href="{{route('comics.show' , $comic )}}"  class="btn btn-success">Ver + Info</a>

  </div>
</div>
@endforeach
</div>
@endsection
