@extends('layouts.master')
@section('titulo')
   Mazinger Comics
@endsection
@section('contenido')

  <div class="container  mb-5 py-3 ">
    <div class="card border-success rounded-3  mb-1 p-3 m-5">
      <div class="row ">
        <div class="col-md-4   ">
           <img src="{{asset('assets/imagenes')}}/{{$comic->logo}}"  class="w-100"/>
          </div>
          <div class="col-md-8 px-3">
            <div class="card-block px-3">
              <h2 class="card-title">Nombre: {{$comic->nombre}}</h2>
              <h5 class="card-text">Autor:  {{$comic->autor}}</h5>
              <h6 class="card-text">{{$comic->descripcion}} </h6>
                <h6 class="card-text"> Nº Paginas: {{$comic->paginas}}</h6>
              <p class="card-text"> Tipografia: {{$comic->tipografia}} </p>
                  <a href="{{route('comics.edit' , $comic )}}" class="btn btn-success btn-lg">Editar</a>
            </div>
          </div>

        </div>
      </div>
    </div>

@endsection

