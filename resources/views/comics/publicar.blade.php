@extends('layouts.master')
@section('titulo')
    Mazinger Comics
@endsection
@section('contenido')
<br>
<br>
<br>
    <div class="row">
 <div class="offset-md-3 col-md-6">
 <div class="card">
 <div class="card-header text-center">
     <h2>Publica tu comic</h2>
 </div>
 <div class="card-body" style="padding:30px">
     

 <form method="post" action="{{route('comics.store') }}" enctype="multipart/form-data">
 @csrf
 <div class="form-group">

 <label for="nombre">Nombre</label>
 <input type="text" name="nombre" id="nombre"  class="form-control" required>
 </div>
 <div class="form-group">

 <label for="autor">Autor</label>
 <input type="text" name="autor" id="autor"  class="form-control" required>
 </div>
 <div class="form-group">

 <label for="tipografia">Tipografia</label>
 <input type="text" name="tipografia" id="tipografia"  class="form-control" required>
 </div>
 
 <div class="form-group">
 <label for="categoria">Categoria</label>

 <select class="form-select form-select-lg mb-3" aria-label=".form-select-lg example" id="categoria" name="categoria">
     @foreach(\App\Models\Categoria::all() as $cat)
     <option value="{{$cat->id}}">{{$cat->nombre}}</option>
     @endforeach
 </select>

 </div>
 
 <div class="form-group">
     
 <label for="tag">Tags</label>

 <select id="tag" class="form-select form-select-lg mb-3" aria-label=".form-select-lg example" name="tag">
     @foreach(\App\Models\Tag::all() as $tag)
     <option value="{{$tag->id}}">{{$tag->nombre}}</option>
     @endforeach
 </select>
 </div>

 <div class="form-group">
 <label for="paginas">Paginas</label>
 <input type="number" name="paginas" id="paginas"  class="form-control" required>
 </div>
 
 <div class="form-group">

 <label for="descripcion">Descripcion</label>

 <textarea class="form-control" id="descripcion" name="descripcion" class="form-control" required rows="3" "></textarea>
 </div>

 <div class="form-group">
 <label for="logo">Imagen</label>
 <input type="file" name="logo"  id="logo" class="form-control" required>
 </div>
 
 </div>
 <div class="form-group text-center">
 <button type="submit" class="btn btn-success" style="padding:8px 100px;margin-top:25px;">
 Publicar
 </button>
 </div>

 </form>
 </div>
 </div>
 </div>

@endsection




