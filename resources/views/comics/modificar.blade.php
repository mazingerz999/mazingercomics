@extends('layouts.master')
@section('titulo')
    Mazinger Comics
@endsection
@section('contenido')
<br>
    <div class="row">
 <div class="offset-md-3 col-md-6">
 <div class="card">
 <div class="card-header text-center">
     <h2> Editar Comic</h2>
 </div>
 <div class="card-body" style="padding:30px">

 <form method="post" action="{{ route('comics.update', $comic) }}" enctype="multipart/form-data">
 
 @csrf 
 @method('put')
 <div class="form-group">
     
     
 <label for="especie">Nombre</label>
 <input type="text" name="nombre" id="nombre" value="{{$comic->nombre}}" class="form-control" required>
 </div>
 <div class="form-group">

 <label for="peso">Autor</label>
 <input type="text" name="autor" id="autor" value="{{$comic->autor}}" class="form-control" required>
 </div>
 <div class="form-group">

 <label for="altura">Paginas</label>
 <input type="number" name="paginas" id="paginas" value="{{$comic->paginas}}" class="form-control" required>
 </div>
 <div class="form-group">

 <label for="descripcion">Descripcion</label>

 <textarea class="form-control" id="descripcion" name="descripcion" rows="3" value="{{$comic->descripcion}}">{{$comic->descripcion}}</textarea>
 </div>
 <div class="form-group">


 <label for="fecha">Tipografia</label>
 <input type="text" name="tipografia" id="tipografia" value="{{$comic->tipografia}}" class="form-control" required>
 </div>
 <div class="form-group">


 <label for="logo">Imagen</label>
 <input type="file" name="logo" id="logo" value="{{$comic->logo}}" class="form-control" required>
 </div>

 </div>
 <div class="form-group text-center">
 <button type="submit" class="btn btn-success" style="padding:8px 100px;margin-top:25px;">
 Editar
 </button>
 </div>
 
 </form>
 </div>
 </div>
 </div>
   
@endsection
