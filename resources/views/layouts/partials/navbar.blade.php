<link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Open+Sans:400,600">
<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.0/css/bootstrap.min.css">
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
<script src="https://code.jquery.com/jquery-3.5.1.min.js"></script>
<script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js"></script>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.5.0/js/bootstrap.min.js"></script>

<nav class="navbar navbar-expand-lg navbar-light bg-light">
	<a href="{{route('comics.index')}}" class="navbar-brand">Mazinger<b>Comics</b></a>  		
	<button type="button" class="navbar-toggler" data-toggle="collapse" data-target="#navbarCollapse">
		<span class="navbar-toggler-icon"></span>
	</button>
	<!-- Collection of nav links, forms, and other content for toggling -->
	<div id="navbarCollapse" class="collapse navbar-collapse justify-content-start">
		<div class="navbar-nav">
			<a href="{{route('comics.index')}}" class="nav-item nav-link">Inicio</a>
					
			<div class="nav-item dropdown">
				<a href="#" data-toggle="dropdown" class="nav-item nav-link active dropdown-toggle">Manga</a>
				<div class="dropdown-menu">
                                    
                                    <input type="hidden" value="{{$cat=App\Models\Categoria::all()->skip(0)->take(5)}}">
                                    <input type="hidden" value="{{$cat2=App\Models\Categoria::all()->skip(5)->take(10)}}">
                                 
                                    
                                    
                                    @foreach($cat as $categoria)
					<a href="{{route('comics.categorias', $categoria)}}" class="dropdown-item">{{$categoria->nombre}}</a>
                                     @endforeach()   
				
				</div>
            </div>
			<div class="nav-item dropdown">
				<a href="#" data-toggle="dropdown" class="nav-item nav-link active dropdown-toggle">Comic</a>
				<div class="dropdown-menu">					
			  @foreach($cat2 as $categoria)
					<a href="{{route('comics.categorias', $categoria)}}" class="dropdown-item">{{$categoria->nombre}}</a>
                                     @endforeach()
				</div>
            </div>

			<a href="{{route('comics.create')}}" class="nav-item nav-link active">Publicar</a>
         @if(Auth::check())
                        <a href="#" class="nav-item nav-link active">Editar tu comic</a>
                 @endif 
                </div> 
            
            <!-- comment -->
            <!-- comment -->
            
            <form class="navbar-form form-inline search-form " method="POST" action="{{route("comics.busqueda")}}">
                @csrf
			<div class="input-group">
                            <input type="text" name="buscador" class="form-control"  placeholder="Que buscas?...">
				<span class="input-group-btn">
					<button type="button" class="btn btn-default"><i class="fa fa-search"></i></button>
				</span>
			</div>
		</form>           
            <!-- comment -->
            <!-- comment -->
            
                @if(Auth::check())
              
              <div class="navbar-nav ml-auto">
			<div class="nav-item dropdown login-dropdown">
				<a href="{{ route('profile.show') }}" class="nav-item nav-link dropdown-toggle"><i class="fa fa-user-o"></i> {{ Auth::user()->name }}</a>
            </a>
                @else
             
		<div class="navbar-nav ml-auto">
			<div class="nav-item dropdown login-dropdown">
				<a href="{{url('login')}}" class="nav-item nav-link dropdown-toggle"><i class="fa fa-user-o"></i> Login</a>
	 	@endif 	
            </div>			
        </div>
	</div>
</nav>
